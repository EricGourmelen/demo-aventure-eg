const pictureElements = document.querySelectorAll('.image__slider');
let activePicture = 0;

// ajout du boutton previous
let btnPrevious = document.createElement("button");
btnPrevious.innerHTML = "<<";
btnPrevious.className = "btnPrevious";
btnPrevious.addEventListener('click', function(event){
    activePicture--;
    if (activePicture < 0){
        activePicture = pictureElements.length-1;
    }
    actualizePage();
})

// ajout du boutton next
let btnNext = document.createElement("button");
btnNext.innerHTML = ">>";
btnNext.className = "btnNext";
btnNext.addEventListener('click', function(event){
    activePicture++;
    if (activePicture > pictureElements.length -1){
        activePicture = 0;
    }
    actualizePage();
})

// ajout des points
let divSelectPicture = document.createElement("div");
divSelectPicture.classList.add('button-container');
document.querySelector('.image__slider').appendChild(divSelectPicture);
pictureElements.forEach(function (img, i){
    let selectButtonPicture = document.createElement("button");
    selectButtonPicture.className = 'round-button';
    selectButtonPicture.addEventListener('click', function(event){
        console.log("je susi dans l'event du bouton rond, i = " + i);
        activePicture = i;
        actualizePage();
    })
    document.querySelector('.button-container').appendChild(selectButtonPicture);
});

actualizePage();

// actualisationd de la page
function actualizePage(){
    pictureElements.forEach(function (img, i){
        if( i === activePicture){
            img.style.opacity = 1;
            img.style.position = 'relative';
            img.appendChild(btnPrevious);
            img.appendChild(btnNext);
            img.appendChild(divSelectPicture);
        } else {
            img.style.opacity = 0;
            img.style.position = 'absolute';
            //remove(document.querySelectorAll('.image__slider button'));
        }
     });
}

